from itertools import chain
import numpy as np


def one_hot(num, size):
    arr = np.zeros(size)
    if num is not None:
        arr[num] = 1
    return arr

class MCG:
    def __init__(self, cooperation, vocabulary, matrix_size, game_length, both_visible,
                 zero_matrix, random_comm, comm_cost):
        self.cooperation = cooperation
        self.vocabulary = vocabulary
        self.matrix_size = matrix_size
        self.game_length = game_length
        self.both_visible = both_visible
        self.zero_matrix = zero_matrix # First agent has zeroes in his matrix
        self.random_comm = random_comm
        self.comm_cost = comm_cost

    def get_total_input_matrix_size(self):
        return (self.matrix_size**2) * (2 if self.both_visible else 1)

    def get_one_step_memory_size(self):
        return (self.vocabulary * 2 + self.matrix_size)

    def get_memory_repeats(self):
        return (self.game_length - 1)

    def get_memory_size(self):
        # No need for memory space for the last step
        return self.get_memory_repeats() * self.get_one_step_memory_size()

    def init_step(self):
        self.last_action = [None] * 2
        self.last_message = [None] * 2

    def init_episode(self):
        # Variables for episode execution
        self.init_step()
        self.matr = [None] * 2
        self.memory = [None] * 2
        self.message_history = [[] for _ in range(2)]
        self.action_history = [[] for _ in range(2)]
        for i in range(2):
            # Generate random matrix
            self.matr[i] = 3 * np.random.randn(self.matrix_size, self.matrix_size)

        if self.zero_matrix:
            self.matr[0] = np.zeros((self.matrix_size, self.matrix_size))

    def commit_messages(self, msg_a, msg_b):
        msg = [msg_a, msg_b]
        for i in range(2):
            self.last_message[i] = msg[i]

    def commit_actions(self, act_a, act_b):
        act = [act_a, act_b]
        for i in range(2):
            self.last_action[i] = act[i]

    def get_received_message_memory_size(self):
        # Received message one hot vectors are at the end of input
        return self.vocabulary * self.get_memory_repeats()

    def _suffix(self, data, step, fake_received_msg = None):
        assert(step <= 0)
        if step == 0:
            pref = data[-self.get_memory_repeats():]
        else:
            pref = data[-self.get_memory_repeats() + step:step]
        suff = [0] * (self.get_memory_repeats() - len(pref))
        if fake_received_msg is not None:
            assert(len(pref) > 0)
            pref[-1] = fake_received_msg
        return pref + suff

    def get_action_history(self, aidx):
        return self.action_history[aidx]

    def get_message_history(self, aidx, enable_random=False):
        if self.random_comm and enable_random:
            return np.random.randint(self.vocabulary, size=len(self.message_history)).tolist()
        return self.message_history[aidx]

    def get_observation(self, agent_idx, fake_received_msg=None, step=0):
        assert(step <= 0)
        # Memory layout --- [action_history, sent_messages, received_messages]
        other_agent_idx = agent_idx ^ 1
        action_history = map(lambda x: one_hot(x, self.matrix_size), self._suffix(self.get_action_history(agent_idx), step))
        sent_messages = map(lambda x: one_hot(x, self.vocabulary), self._suffix(self.get_message_history(agent_idx), step))

        # For CIC evaluation
        received_messages = map(lambda x: one_hot(x, self.vocabulary),
                                self._suffix(self.get_message_history(other_agent_idx, True), step, fake_received_msg))

        memory = np.concatenate(list(action_history) + list(sent_messages) + list(received_messages))
        visible_matrices = [self.matr[agent_idx]]
        if self.both_visible:
            visible_matrices.append(self.matr[other_agent_idx])
        return (visible_matrices, memory)

    def commit_step(self):
        # Update the state based on both agent's actions
        acts = []
        msgs = []
        rewards = []
        for i in range(2):
            acts.append(one_hot(self.last_action[i], self.matrix_size))
            msgs.append(one_hot(self.last_message[i], self.vocabulary))
            rewards.append(self.matr[i][self.last_action[0]][self.last_action[1]])
            self.message_history[i].append(self.last_message[i])
            self.action_history[i].append(self.last_action[i])

        # Add cooperation into account of reward
        fixed_rewards = [0] * 2
        for i in range(2):
            fixed_rewards[i] = rewards[i] + self.cooperation * rewards[i ^ 1]
        rewards = fixed_rewards

        # Give back reward only in the last step
        if len(self.message_history[0]) != self.game_length:
            rewards = [0 for _ in rewards]

        # Apply cost for communication for all previous steps than the last one
        if self.comm_cost is not None and len(self.message_history[0]) != self.game_length:
            for aidx in range(2):
                if self.message_history[aidx][-1] != 0:
                    rewards[aidx] -= self.comm_cost

        self.init_step()

        return rewards

    def get_last_message(self, idx):
        if len(self.message_history) == 0:
            raise ValueError("Message is not available before first step")
        return self.message_history[idx][-1]


