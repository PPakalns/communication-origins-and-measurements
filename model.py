import os

import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.distributions import Categorical

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
eps = np.finfo(np.float32).eps.item()

class ReinforceAgent(nn.Module):

    def __init__(self, matrix_size, vocabulary, dimension, memory_size, both_visible, one_layer):
        super(ReinforceAgent, self).__init__()

        self.visible_matrices = 2 if both_visible else 1
        self.matrix_size = matrix_size
        self.input_size = self.visible_matrices * matrix_size ** 2
        self.vocabulary = vocabulary
        self.memory_size = memory_size
        self.dimension = dimension
        self.one_layer = one_layer

        n_hid_1 = 20 + self.matrix_size * 5
        n_hid_2 = n_hid_1
        self.l1 = nn.Linear(self.input_size + self.memory_size, n_hid_1)
        self.l2 = nn.Linear(self.l1.out_features, n_hid_2)
        self.l3 = nn.Linear(self.l1.out_features, n_hid_2)
        n_last = self.l3
        if self.one_layer:
            self.r1 = nn.Linear(self.input_size + self.memory_size, 6)
            n_last = self.r1
        self.o_act = nn.Linear(n_last.out_features, self.matrix_size)
        self.o_com = nn.Linear(n_last.out_features, self.vocabulary)
        self.v = nn.Linear(self.l3.out_features, 1)


    def forward(self, matrix:torch.Tensor, memory:torch.Tensor):
        matrix = torch.from_numpy(matrix).view(self.input_size)
        memory = torch.from_numpy(memory).view(self.memory_size)
        inp = torch.cat([matrix, memory], dim=0).float()
        x = F.relu(self.l1(inp))
        x = F.relu(self.l2(x))
        x = F.relu(self.l3(x))
        v = self.v(x)
        if self.one_layer:
            x = F.relu(self.r1(inp))
        act = self.o_act(x)
        message = self.o_com(x)
        act = F.softmax(act, dim=0)
        message = F.softmax(message, dim=0)
        return [act, message], v


class ReinforceAgentWrapper:

    def __init__(self, matrix_size, vocabulary, dimension, memory_size,
                 both_visible, random_comm, reduce_entropy, one_layer):
        self.model = ReinforceAgent(matrix_size, vocabulary, dimension, memory_size, both_visible, one_layer)
        # self.model.to(device)
        self.optimizer = optim.Adam(self.model.parameters(), lr=0.005)
        self.dimension = dimension
        self.reduce_entropy = reduce_entropy
        self.rewards = []
        # Stores tuple (log_prob, ent_act, ent_com, V(o))
        self.train_history = []
        self.epoch = 0
        self.random_comm = random_comm
        self.policy_loss = []

    def load(self, directory):
        path = os.path.join(directory, f"model{self.dimension}")
        checkpoint = torch.load(path)
        self.model.load_state_dict(checkpoint['model_state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.epoch = checkpoint['epoch']
        self.random_comm = checkpoint['random_comm']
        print(f"Model loaded from {path}. Model is trained for {self.epoch} episodes")

    def save(self, directory):
        os.makedirs(directory, exist_ok=True)
        path = os.path.join(directory, f"model{self.dimension}")
        torch.save({
            'epoch': self.epoch,
            'random_comm': self.random_comm,
            'model_state_dict': self.model.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
        }, path)
        print(f"Model saved at {path}")

    def finish_episode(self, time, is_train, verbose=False):
        if not is_train:
            return
        R = 0
        gamma = 0.9
        policy_loss = []
        returns = []
        for r in self.rewards[::-1]:
            R = r + gamma * R
            returns.append(R)
        returns.reverse()

        for step, values in enumerate(zip(*zip(*self.train_history), returns)):
            log_prob_act, log_prob_msg, \
                e_act, e_com, vo, ret = values
                        # policy loss, entropy loss, V(o) estimation loss

            last_step = (step == len(returns) - 1)
            # msg loss only when messages are relevant
            msg_coef = 0 if last_step else 1

            # action entropy loss only when action is relevant
            # action is relevant because it is given later as input to agent
            act_coef = 1 #0 if not last_step else 1

            log_loss = (-log_prob_act + (msg_coef * (-log_prob_msg))) * (ret - vo)

            ent_loss = -(act_coef * 0.4 * e_act + msg_coef * 0.2 * e_com)

            # Reduce entropy over training
            if self.reduce_entropy:
                if time >= 0.9:
                    ent_loss = 0
                elif time > 0.6:
                    ent_loss = ent_loss * ((0.9 - time) / 0.3)

            base_loss = (ret - vo)**2
            iter_loss = log_loss + ent_loss + base_loss
            self.policy_loss.append(iter_loss.unsqueeze(0))
            if verbose:
                print("RET", ret)
                print("VO", vo)
                print("ACT_E", log_loss)
                print("ENT_E", ent_loss)
                print(" VO_E", base_loss)
        self.epoch += 1

    def update_gradients(self):
        self.optimizer.zero_grad()
        policy_loss = torch.cat(self.policy_loss).mean()
        policy_loss.backward()
        self.optimizer.step()
        self.policy_loss = []

    def reset_episode(self):
        self.rewards.clear()
        self.train_history.clear()

    def get_input_norm(self, source, sz):
        if source == 'input':
            W = list(self.model.l1.parameters())[0].data.numpy()
            # Input takes up the first sz input elements
            return np.linalg.norm(W[:,:sz])
        elif source == 'message':
            W = list(self.model.l1.parameters())[0].data.numpy()
            return np.linalg.norm(W[:,-sz:])
        else:
            raise ValueError(f'Unexpected source type {source}')

    def get_probs(self, state):
        matr = np.concatenate(state[0]).flatten()
        memory = state[1]
        with torch.set_grad_enabled(False):
            probs, v = self.model(matr, memory)
            p_probs, p_message = probs
        return p_probs, p_message

    def action(self, state, is_train):
        matr = np.concatenate(state[0]).flatten()
        memory = state[1]
        with torch.set_grad_enabled(is_train):
            probs, v = self.model(matr, memory)
            log_probs = []
            entropy = []
            choices = []
            for prob in probs:
                m = Categorical(prob)
                # if is_train and np.random.rand() < 0.001:
                #     choice = torch.tensor(np.random.randint(prob.shape[0]))
                # else:
                choice = m.sample()
                # choices.append(choice.to("cpu"))
                choices.append(choice)
                log_probs.append(m.log_prob(choice))
                prob = torch.clamp(prob, 1e-6, 1)
                ent = (-prob * torch.log(prob))
                assert(ent.shape == prob.shape)
                entropy.append(ent.sum(dim=0))
            self.train_history.append((*log_probs, *entropy, v))
        return choices

    def reward(self, reward):
        self.rewards.append(reward)
