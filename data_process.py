#!/usr/bin/env python3

import os
import csv
import argparse

def calculate_running_average(data, window):
    total = 0
    cnt = 0
    output = []
    for i in range(0, len(data)):
        total += data[i]
        cnt += 1
        if cnt > window:
            cnt -= 1
            total -= data[i - window]
        output.append(total / cnt)
    return output

def drop_unneeded(output, points_to_leave):
    if len(output) <= points_to_leave:
        return output
    block_size = len(output) // points_to_leave
    return output[block_size - 1::block_size]

def process_running_average(args):
    print("Processing reward")
    with open(args.source_file, 'r') as f:
        print(f"Reading {args.source_file}")
        csv_reader = csv.reader(f, delimiter=',')
        rows = []
        for row in csv_reader:
            rows.append([float(val) for val in row])
    data = list(zip(*rows))
    output = {}
    output['x'] = [i + 1 for i in range(len(data[0]))]
    if args.store_raw:
        for i in range(len(data)):
            output[f"{args.prefix}_{i}_raw"] = data[i]
    for i in range(len(data)):
        output[f"{args.prefix}_{i}"] = calculate_running_average(data[i], args.window)

    for key in output.keys():
        output[key] = drop_unneeded(output[key], args.data_points)

    with open(args.target_file, 'w') as f:
        print(f"Writing {args.target_file}")
        writer = csv.writer(f)
        writer.writerow(output.keys())
        writer.writerows(zip(*output.values()))
    print("DONE")

def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title='Generated data processing commands', dest='subparser')
    subparsers.required = True
    reward_average = subparsers.add_parser('reward-average')
    reward_average.add_argument('source_file', type=str, help="Path to reward csv")
    reward_average.add_argument('target_file', type=str)
    reward_average.add_argument('prefix', type=str)
    reward_average.add_argument('-store-raw', action='store_true')
    reward_average.add_argument('-window', type=int, default=1000, help='Size of running average window')
    reward_average.add_argument('-data_points', type=int, default=500, help='Data points to draw')
    reward_average.set_defaults(func=process_running_average)
    opts = parser.parse_args()
    opts.func(opts)

if __name__ == "__main__":
    main()

