import math

import numpy as np
from mcg import MCG

def prob_from_occurences(occ, size):
    cnt = [0]*size
    if not occ:
        return cnt
    for o in occ:
        cnt[o] += 1
    total = len(occ)
    return [val / total for val in cnt]

def count_co_occurences(a, b, size_a, size_b):
    mat = np.zeros((size_a, size_b))
    for ax, bx in zip(a, b):
        mat[ax][bx] += 1
    return mat

def mutual_information(a, b, size_a, size_b):
    # a, b stores occurence lists, size defines the possible occurence values
    p_a = prob_from_occurences(a, size_a)
    p_b = prob_from_occurences(b, size_b)
    p_ab = count_co_occurences(a, b, size_a, size_b) / len(a)

    mutual_information = 0
    for a in range(size_a):
        for b in range(size_b):
            if p_ab[a][b] > 0:
                mutual_information += p_ab[a][b] * math.log(p_ab[a][b] / (p_a[a] * p_b[b]))
    return mutual_information

def entropy(a, size_a):
    # Bad metric - should be called for specific type of inputs etc
    p_a = prob_from_occurences(a, size_a)
    entropy = 0
    for i in range(size_a):
        if p_a[i] > 1e-9:
            entropy += p_a[i] * -math.log(p_a[i])
    return entropy

def prob_matrices_given_a(a, b, size_a, size_b):
    # Calculates p(B | A) for all A in a, B in b pairs
    p_a = prob_from_occurences(a, size_a)

    # Calculate p(B | A)
    # prob - row sums to 1, has fixed A
    # Could divide probability matrix with vector, but doesn't handle zeroes very well
    prob = np.zeros((size_a, size_b))
    assert len(a) == len(b)
    for i in range(size_a):
        filtered_occ = []
        for oc_a, oc_b in filter(lambda x: x[0] == i, zip(a, b)):
            assert(oc_a == i)
            filtered_occ.append(oc_b)
        occ = prob_from_occurences(filtered_occ, size_b)
        prob[i,:] = occ
    return p_a, prob

def context_independence(a, b, size_a, size_b):
    # a = message, b = concepts(actions)
    p_a = np.array(prob_from_occurences(a, size_a))
    p_b = np.array(prob_from_occurences(b, size_b))
    p_ab = count_co_occurences(a, b, size_a, size_b) / len(a)

    # p_a_b means p_ab(a | b) (Conditional probability)
    # 1e-9 # do not raise error when dividing
    p_a_b = np.divide(p_ab, np.reshape(p_b + 1e-9, (1, -1)))
    p_b_a = np.divide(p_ab, np.reshape(p_a + 1e-9, (-1, 1)))

    # a - axis 0, b - axis 1
    # maxm_b store Vb : a_b = arg max_a p_b_a(b | a)
    maxm_b = np.argmax(p_b_a, axis=0)
    assert(maxm_b.shape[0] == size_b)

    ci = 0
    for b_val in range(size_b):
        ci += p_a_b[maxm_b[b_val]][b_val] * p_b_a[maxm_b[b_val]][b_val]
    ci /= size_b
    return ci


def calc_cic_from_probs(env, msg_probs, action_probs):
    assert(msg_probs.size == env.vocabulary)
    assert(action_probs.shape == (env.vocabulary, env.matrix_size))

    p_m_a = np.zeros((env.vocabulary, env.matrix_size))
    for msg in range(env.vocabulary):
        for act in range(env.matrix_size):
            p_m_a[msg][act] = action_probs[msg][act] * msg_probs[msg]
    p_a = np.sum(p_m_a, axis=0)
    assert(p_a.size == env.matrix_size)

    p_m = msg_probs

    cic = 0
    for msg in range(env.vocabulary):
        for act in range(env.matrix_size):
            if p_m_a[msg][act] > 0:
                cic += p_m_a[msg][act] * math.log(p_m_a[msg][act] / (p_a[act] * p_m[msg]))
    return cic


def calc_cic(env:MCG, agents):
    cic = []
    for cic_aidx in range(len(agents)):
        other_aidx = cic_aidx ^ 1
        _, other_c_prob = agents[other_aidx].get_probs(env.get_observation(other_aidx, step=-1))
        action_probs = []
        for msg in range(env.vocabulary):
            action_prob, _ = agents[cic_aidx].get_probs(env.get_observation(cic_aidx, msg))
            action_probs.append(action_prob.data.numpy())
        cic.append(calc_cic_from_probs(env, other_c_prob.data.numpy(), np.array(action_probs)))
    return cic

