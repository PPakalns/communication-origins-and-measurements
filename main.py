#!/usr/bin/env python3

import sys
import os
import csv
import argparse
import operator
import functools
import datetime as dt
import pickle
import numpy as np

import mcg
from model import ReinforceAgentWrapper
from metrics import mutual_information, entropy, prob_matrices_given_a,\
                    context_independence, calc_cic
from utility import verbose_matrix, verbose_row, verbose_metrics

class DataStore:
    def __init__(self, name):
        self.name = name
        self.data = []

    def load(self, directory):
        print("Loading ", self.name)
        with open(os.path.join(directory, self.name), 'rb') as f:
            self.data = pickle.load(f)

    def save(self, directory):
        print("Saving ", self.name)
        os.makedirs(directory, exist_ok=True)
        with open(os.path.join(directory, self.name), 'wb') as f:
            pickle.dump(self.data, f)

    def save_txt(self, directory):
        print("Saving csv ", self.name)
        os.makedirs(directory, exist_ok=True)
        txt_name = self.name + '.csv'
        with open(os.path.join(directory, txt_name), 'w') as f:
            csvWriter = csv.writer(f, delimiter=',')
            csvWriter.writerows(self.data)

    def append(self, val):
        self.data.append(val)


def execute(opts):
    env = mcg.MCG(
        cooperation = opts.cooperation,
        vocabulary = opts.vocabulary,
        matrix_size = opts.matrix_size,
        game_length = opts.game_length,
        both_visible = opts.both_visible,
        zero_matrix = opts.zero_matrix,
        random_comm = opts.random_comm,
        comm_cost = opts.comm_cost,
    )

    agents = [
        ReinforceAgentWrapper(
            matrix_size = opts.matrix_size,
            vocabulary = opts.vocabulary,
            dimension = dimension,
            memory_size = env.get_memory_size(),
            both_visible = opts.both_visible,
            random_comm = opts.random_comm,
            reduce_entropy = opts.reduce_entropy,
            one_layer = opts.one_layer,
        ) for dimension in range(2)
    ]

    store_reward = DataStore('reward')
    store_reward_step = DataStore('reward_step')

    is_train = not opts.evaluate

    if opts.load_dir:
        for agent in agents:
            agent.load(opts.load_dir)
        store_reward.load(opts.load_dir)
        store_reward_step.load(opts.load_dir)

    start_time = dt.datetime.today().timestamp()
    running_reward = [0, 0]

    sc_occurences = [[] for _ in range(2)]
    ic_occurences = [[] for _ in range(2)]
    cic = []
    reward_now = []

    for episode_idx in range(1, opts.episode_cnt + 1):
        env.init_episode()
        step_reward = []
        for step in range(opts.game_length):
            actions, communications, inp_communications = [], [], []
            for a_idx, agent in enumerate(agents):
                act, com = agent.action(env.get_observation(a_idx), is_train)
                actions.append(act.data.item())
                communications.append(com.data.item())
                if step > 0:
                    inp_communications.append(env.get_last_message(a_idx ^ 1))

            # Hijack game state for CIC measurement
            if opts.evaluate and step == opts.game_length - 1:
                cic.append(calc_cic(env, agents))

            env.commit_messages(*communications)
            env.commit_actions(*actions)
            rewards = env.commit_step()
            step_reward.append(rewards)
            for a_idx in range(len(agents)):
                agents[a_idx].reward(rewards[a_idx])

            if opts.evaluate and step > 0:
                # In first step agents doesn't receive message
                # Store data for speaker consistency evaluation
                if step == opts.game_length - 2:
                    for idx, s in enumerate(zip(communications, actions)):
                        sc_occurences[idx].append(s)

                if step > 0: #== opts.game_length - 1
                    # Store data for instantaneous coordination
                    for idx, s in enumerate(zip(inp_communications, actions)):
                        ic_occurences[idx].append(s)

        verbose_step = episode_idx == opts.episode_cnt or (episode_idx & (2**10 - 1)) == 0

        # Finish episode
        for agent in agents:
            agent.finish_episode(episode_idx / opts.episode_cnt, is_train, verbose_step)
            agent.reset_episode()

        # Update agent gradients in batch
        if is_train and (episode_idx == opts.episode_cnt or episode_idx % opts.batch_size == 0):
            for agent in agents:
                agent.update_gradients()

        total_reward = [sum([val[idx] for val in step_reward]) for idx in range(2)]
        running_coef = 0.001
        running_reward = [running_coef * total_reward[idx] + (1 - running_coef) * running_reward[idx]
                          for idx in range(2)]

        reward_now.append(total_reward)
        store_reward.append(total_reward)
        store_reward_step.append(step_reward)

        if verbose_step:
            print("----------------")
            print("Episode ", episode_idx)
            print("Observations:")
            for idx in range(2):
                for z in env.get_observation(idx):
                    if isinstance(z, list):
                        for zz in z:
                            print(zz)
                    else:
                        print(z)
            print("Rewards ", rewards)
            print("Last action", actions)
            print("----------------")
        if episode_idx == opts.episode_cnt or (episode_idx & (2**8 - 1)) == 0:
            per_sec = episode_idx / (dt.datetime.today().timestamp() - start_time)
            till_end = (opts.episode_cnt + 1 - episode_idx) / per_sec
            print(f"{running_reward[0]:8.4f} {running_reward[1]:8.4f} {total_reward[0]:8.4f} {total_reward[1]:8.4f} {per_sec:8.4f} {till_end:8.4f}")

    if opts.evaluate:
        print("Evaluated metrics")
        total_avg_reward = list(map(lambda x: x / len(reward_now), map(sum, zip(*reward_now))))
        cic_calc = np.sum(np.array(cic), axis=0) / len(cic)
        for idx, agent in enumerate(agents):
            print(f"==== Agent {idx} ====")
            sc = mutual_information(*zip(*sc_occurences[idx]), opts.vocabulary, opts.matrix_size)
            ic = mutual_information(*zip(*ic_occurences[idx]), opts.vocabulary, opts.matrix_size)
            ci = context_independence(*zip(*sc_occurences[idx]), opts.vocabulary, opts.matrix_size)
            print("Total average reward", total_avg_reward[idx])
            print("Speaker consistency", sc)
            print("Instantenious coordination", ic)
            output_messages, _ = zip(*sc_occurences[idx])
            msg_entropy = entropy(output_messages, opts.vocabulary)
            print("Message entropy", msg_entropy)
            print("Context independence", ci)
            print("First neural network layer weight normal length")
            X_norm_mess = agent.get_input_norm(source='message', sz=env.get_received_message_memory_size())
            X_norm_input = agent.get_input_norm(source='input', sz=env.get_total_input_matrix_size())
            print("||X_message||", X_norm_mess)
            print("||X_input||", X_norm_input)
            assert(cic_calc.size == 2)
            print("Causual influence of communication", cic_calc[idx])
            metrics = [total_avg_reward[idx], sc, ic, msg_entropy, ci, X_norm_mess, X_norm_input, cic_calc[idx]]
            print("Qualative matrices")
            p_c, p_ac = prob_matrices_given_a(*zip(*sc_occurences[idx]), opts.vocabulary, opts.matrix_size)
            print("Action probabilities if message sent")
            print("p_c_out", p_c)
            print(p_ac)
            p_c_in, p_ac_in = prob_matrices_given_a(*zip(*ic_occurences[idx]), opts.vocabulary, opts.matrix_size)
            print("Action probabilities if message received")
            print("p_c_in", p_c_in)
            print(p_ac_in)
            if opts.verbose:
                verbose_metrics(metrics)
                verbose_row(p_c)
                verbose_matrix(p_ac)
                verbose_row(p_c_in)
                verbose_matrix(p_ac_in)
        print("=====================")

    for agent in agents:
        print(f"Agent learned for {agent.epoch} iterations")
        agent.save(opts.save_dir)

    if not opts.evaluate:
        store_reward.save(opts.save_dir)
        store_reward.save_txt(opts.save_dir)
        store_reward_step.save(opts.save_dir)
        store_reward_step.save_txt(opts.save_dir)
        with open(os.path.join(opts.save_dir, "args.txt"), 'w') as f:
            print(opts, file=f)
        with open(os.path.join(opts.save_dir, "cargs.txt"), 'w') as f:
            print(' '.join(sys.argv), file=f)

    print("Learning finished")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-zero-matrix", action='store_true', help="Matrix of the first agent always contains zeroes")
    parser.add_argument("-random-comm", action='store_true', help="Replace input communication with random messages")
    parser.add_argument("-comm-cost", type=float, help="Apply additional cost for sending nonzero message")
    parser.add_argument("-reduce-entropy", action='store_true', help="Reduce entropy reward over training time to zero")
    parser.add_argument("-one-layer", action='store_true', help="Only one hidden layer")

    parser.add_argument("-load-dir", type=str, help="Directory from which to load existing model from.")
    parser.add_argument("-save-dir", type=str, required=True, help="Directory to which model will be stored.")
    parser.add_argument("-episode-cnt", type=int, default=50000, help="Agent training episode count")
    parser.add_argument("-batch-size", type=int, default=128, help="Batch size")
    parser.add_argument("-matrix-size", type=int, default=4, help="Matrix size of MCG game")
    parser.add_argument("-vocabulary", type=int, default=5, help="Discrete symbol count for communication")
    parser.add_argument("-game-length", type=int, default=3, help="Steps in one game")
    parser.add_argument("-cooperation", type=float, default=0, help="How much reward does agent get from the reward received by other agent")
    parser.add_argument("-both-visible", action='store_true', help="Are both matrices visible to both agents (by default they see only their own matrix)")
    parser.add_argument("-evaluate", action='store_true', help="Do evaluation of model -- calculate a lot of different metrics")
    parser.add_argument("-verbose", action='store_true', help="Evaluation output will be more Latex friendly")
    opts = parser.parse_args()
    if opts.evaluate and not opts.load_dir:
        raise Exception("Load dir must be specified when -evaluate is given")
    execute(opts)


if __name__ == "__main__":
    main()

