#!/usr/bin/env python3

import numpy as np
import statistics

TEST_COUNT = 100
WINDOW = 1000

def max_val_test(shape, mean, std, test_count, summands = 1):
    max_val = []
    for i in range(test_count):
        val = []
        for j in range(WINDOW):
            ss = np.zeros(shape)
            for _ in range(summands):
                ss += np.random.normal(mean, std, size = shape)
            val.append(np.max(ss))
        max_val.append(statistics.mean(val))
    return statistics.mean(max_val), statistics.stdev(max_val)

for sz in range(3, 5):
    for row in range(1, sz + 1):
        print(f"Sagaidama vidaja vertiba maksimumam no ({sz} x {row}) matricas",
              max_val_test((sz, row), 0, 3, TEST_COUNT))

print("Sagaidamas vertibas (nulles matricas)")
for sz in range(1, 10):
    vals = max_val_test((sz,), 0, 3, TEST_COUNT)
    vals = [round(val, 3) for val in vals]
    print(f"{sz}\t{vals}")

print("Sagaidamas vertibas")
for sz in range(1, 10):
    vals = max_val_test((sz,), 0, 3, TEST_COUNT, summands = 2)
    vals = [round(val, 3) for val in vals]
    print(f"{sz}\t{vals}")
