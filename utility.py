
# Functions for processed data to be easy to use in latex document

def verbose_matrix(matr):
    print("==== VERBOSE")
    for x in range(matr.shape[1]):
        for y in range(matr.shape[0]):
            print(x, y, matr[y][x])
        print()
    print("----")

def verbose_row(matr):
    print("==== VERBOSE")
    for z in range(2):
        for x in range(len(matr)):
            print(z, x, matr[x])
        print()
    print("----")

def verbose_metrics(vals):
    print("==== VERBOSE")
    for idx, val in enumerate(vals):
        print(val, "\\\\" if idx == len(vals) - 1 else "&")
    print("----")

